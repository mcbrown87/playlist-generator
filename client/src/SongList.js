import React from 'react';
import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';

export default class SongList extends React.Component {
    render() {
      return (
        <div>
          <Grid item xs>
            <List
              id={this.props.id}
              component="nav"
              subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                </ListSubheader>
              }
            >
              {this.props.songs.map((x, i) => 
                  <ListItem 
                    button
                    selected={this.props.selected === i}
                    onClick={this.props.onClick ? (event) => this.props.onClick(event, i) : null}
                    disabled={x.disabled}
                  >
                    <ListItemAvatar>
                      <Avatar src={x.track.album.images[1].url} />
                    </ListItemAvatar>
                    <ListItemText primary={`${x.track.name} - ${x.track.artists[0].name}`} />
                  </ListItem>
              )}
            </List>
          </Grid>
        </div>
      )
    }
  }
  