import React from 'react';
import TextField from "@mui/material/TextField"
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Backdrop from '@mui/material/Backdrop';
import MoonLoader from "react-spinners/MoonLoader";
import Enumerable from 'linq'
import SongList from './SongList'

const controller = new AbortController();
const { signal } = controller.signal;

export default class SongOfTheMorningPlaylist extends React.Component {
    constructor() {
      super()
      this.searchPattern = ""
      this.state = {
        searchResults: [],
        selectedTrack: null,
        searching: false,
        selectedPlaylist: 0,
        busy: false,
        playlists: []
      }

      fetch('getPlaylists')
        .then(res => res.json()) 
        .then(res => {
          this.setState(() => ({
            playlists: res
          })) 
      }) 

      this.handleArtistInput = this.handleArtistInput.bind(this)
      this.handleListItemClick = this.handleListItemClick.bind(this)
      this.addSong = this.addSong.bind(this)
      this.onPlaylistSelect = this.onPlaylistSelect.bind(this)
      this.onCreateCleanPlaylist = this.onCreateCleanPlaylist.bind(this)
    }
  
    async onCreateCleanPlaylist() {
      await fetch(`createCleanPlaylist?targetPlaylistId=${this.state.playlists[this.state.selectedPlaylist].id}`, {method: 'POST'})
    }

    async onPlaylistSelect(event) {
      let selectedPlaylist = event.target.value
      this.setState(() => ({
        selectedPlaylist: selectedPlaylist,
        busy: true
      }))

      await this.updateSearchResults(this.searchPattern)

      this.setState(() => ({
        busy: false
      }))
    }

    async addSong() {
      this.setState(() => ({
        busy: true
      }))
      
      await fetch(`addSongToPlaylist?playlistId=${this.state.playlists[this.state.selectedPlaylist].id}&trackId=${this.state.searchResults[this.state.selectedTrack].track.id}`, {method: 'POST'})
      await this.updateSearchResults(this.searchPattern)

      this.setState(() => ({
        busy: false,
        selectedTrack: null
      }))
    }

    handleListItemClick(event, index) {
      let selectedTrack = index
      this.setState(() => ({
        selectedTrack: selectedTrack
      }))
    }

    async handleArtistInput(e) {
      await this.updateSearchResults(e.target.value)
    }

    async updateSearchResults(searchPattern) {
      try {
        this.searchPattern = searchPattern
        if (searchPattern === '') {
          this.setState(() => ({
            searchResults: []
          }))

          return 
        }

        this.setState(() => ({
          searching: true
        }))

        let results = await fetch(`/searchMusic?searchPattern=${searchPattern}`, {signal}).then(res => res.json())
        let artistsInPlaylist = Enumerable.from((await fetch(`/getArtistsInPlaylist?playlistId=${this.state.playlists[this.state.selectedPlaylist].id}`).then(res => res.json())))

        this.setState(() => ({
          searchResults: Enumerable.from(results).select(x => {
            return {  
              track: x, 
              disabled: artistsInPlaylist.contains(x.artists[0].name)
            }
          }).toArray()

        }))
      } finally {
        this.setState(() => ({
          searching: false
        }))
      }
    }

    render() {
      return (
        <div>
          <Grid 
            container 
            spacing={2}
            direction="column"
            justify="flex-start"
            alignItems="center"
          >
            <Grid item xs>
              <FormControl variant="outlined">
                <InputLabel>Select Playlist</InputLabel>
                <Select
                  value={this.state.selectedPlaylist}
                  onChange={this.onPlaylistSelect}
                  label="Select Playlist"
                >
                  {this.state.playlists.map((x, i) => 
                    <MenuItem value={i}>{x.name}</MenuItem>
                  )}
                </Select>
                <Button 
                variant="contained"
                onClick={this.onCreateCleanPlaylist}
                id="createCleanPlaylist"
                 >Create Clean Version</Button>
              </FormControl>
            </Grid>
            <Grid item xs>
              <TextField 
                id="search" 
                variant="outlined"
                label="Search for music"
                onChange={this.handleArtistInput}
              />
            </Grid>
            <Grid item xs>
              <Button 
                variant="contained"
                onClick={this.addSong}
                disabled={this.state.selectedTrack == null}
                id="addSongButton"
              >Add to playlist</Button>
            </Grid>
            <Grid item xs>
              <SongList 
                selected={this.state.selectedTrack}
                onClick={(event, i) => this.handleListItemClick(event, i)}
                songs={this.state.searchResults}
              />
            </Grid>
            <Backdrop open={this.state.busy}>
              <MoonLoader
                  id="spinner"
                  size={50}
                  color={"#123abc"}
                  loading={this.state.busy}
              />
            </Backdrop>
          </Grid>
        </div>
      )
    }
  }
  