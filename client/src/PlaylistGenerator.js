import React from 'react';
import MoonLoader from "react-spinners/MoonLoader";
import TextField from "@mui/material/TextField"
import Typography from '@mui/material/Typography';
import Slider from '@mui/material/Slider';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button'
import Switch from '@mui/material/Switch';
import Autocomplete from '@mui/material/Autocomplete';
import soundFile from './Hi-Hat-Closed-Hit-A2-www.fesliyanstudios.com.mp3'
import {Howl} from 'howler';
import SongList from './SongList'

export default class PlaylistGenerator extends React.Component {
    constructor() {
      super()
      this.defaultUpperTempo = 175
      this.defaultLowerTempo = 110
      this.metronomeTicker = null
      this.tickSound = new Howl({
        src: [soundFile]
      })
      this.state = {
        busy: false,
        upperTempo: this.defaultUpperTempo,
        lowerTempo: this.defaultLowerTempo,
        currentTempoValue: [this.defaultUpperTempo],
        playMetronome: false,
        intervalTraining: false, 
        genres: [],
        songs: []
      }
  
      this.selectedGenre = null
      this.artist = ""
      this.getSongs = this.getSongs.bind(this)
      this.handleArtistInput = this.handleArtistInput.bind(this)
      this.handleTempoInput = this.handleTempoInput.bind(this)
      this.handleMetronomeSwitch = this.handleMetronomeSwitch.bind(this)
      this.handleIntervalTrainingSwitch = this.handleIntervalTrainingSwitch.bind(this)
      this.makePlaylist = this.makePlaylist.bind(this)
    
      fetch('getGenres')
      .then(res => res.json()) 
      .then(res => {
        this.setState(() => ({
          genres: res
        })) 
      }) 
    }

    makePlaylist() {
      this.setState(() => ({
        busy: true
      }))
      
      try {
        let segments = []
        if(this.selectedGenre) {
          segments.push(this.selectedGenre)
        }

        if (this.artist) {
          segments.push(this.artist)
        }

        let playlistName = `${segments.join(",")}(${this.state.upperTempo}${this.state.intervalTraining ? "-" + this.state.lowerTempo : ""} BPM)`
        fetch(`savePlaylist?songs=${this.state.songs.map(x => x.id).join(',')}&playlistName=${playlistName}`, {method: 'POST'})
      } finally  {
        this.setState(() => ({
          busy: false,
          songs: []
          }))
      }
    }
  
    async handleArtistInput(e) {
      if (e.keyCode === 13 && !this.state.busy) {
        await this.getSongs()
      } else {
        this.artist = e.target.value
      }
    }
  
    handleTempoInput(e, newValue) {
      this.setState(() => ({
        lowerTempo: newValue[0],
        upperTempo: newValue.length > 1 ? newValue[1] : newValue[0],
        currentTempoValue : newValue
      }))
      if(this.state.playMetronome) {
        clearInterval(this.metronomeTicker)
        this.metronomeTicker = setInterval(() => this.tickSound.play(), 60000/this.state.upperTempo)
      }
    }
  
    handleMetronomeSwitch(e) {
      this.setState({playMetronome: e.target.checked})
      
      if(e.target.checked) {
        this.metronomeTicker = setInterval(() => this.tickSound.play(), 60000/this.state.upperTempo)
      } else {
        clearInterval(this.metronomeTicker)
      }  
    }

    handleIntervalTrainingSwitch(e) {
      this.setState({
        intervalTraining: e.target.checked,
        currentTempoValue: e.target.checked ? [this.state.upperTempo, this.state.lowerTempo] : [this.state.upperTempo]
        })
    }
  
    async getSongs() {
      this.setState(() => ({
        busy: true,
        songs: []
      }))
  
      try {
        let walkingSongs = []
        let songs = await fetch(`makePlaylist?artist=${this.artist}&genre=${this.selectedGenre}&tempo=${this.state.upperTempo}`).then(res => res.json())
        if(this.state.intervalTraining) {
          walkingSongs = await fetch(`makePlaylist?artist=${this.artist}&genre=${this.selectedGenre}&tempo=${this.state.lowerTempo}`).then(res => res.json())
          for(var i in walkingSongs) {
            if(i < songs.length) {
              let walkingSong = walkingSongs[i]
              songs.splice(i*3, 0, walkingSong)
            }
          }
        }
      
        this.setState(() => ({
          songs: songs
        }))
      } finally {
        this.setState(() => ({
          busy: false
        }))
      }
    }
  
    render() {
      return (
        <div>
          <Grid 
            container 
            spacing={2}
            direction="column"
            justify="flex-start"
            alignItems="center"
          >
            <Grid item xs>
              <Autocomplete
                id="genre"
                options={this.state.genres}
                getOptionLabel={option => option}
                onChange={(event, value) => this.selectedGenre = value}
                style={{ width: 300 }}
                renderInput={params => <TextField {...params} label="Genres" variant="outlined" />}
              />
            </Grid>
            <Grid item xs>
              <TextField 
                id="artist" 
                variant="outlined"
                label="Enter an artist"
                onKeyUp={this.handleArtistInput}
                onChange={this.handleArtistInput}
              />
            </Grid>
            <Grid item xs>
              <Switch
                id="intervalTraining"
                checked={this.state.intervalTraining}
                onChange={this.handleIntervalTrainingSwitch}
                color="primary"
                inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              <Switch
                id="metronomeSwitch"
                checked={this.state.playMetronome}
                onChange={this.handleMetronomeSwitch}
                color="primary"
                inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              <Typography id="discrete-slider-always" align='left' style={{marginRight: "140px"}}>
                Tempo
              </Typography>
              <Slider
                id="tempo"
                defaultValue={[this.defaultUpperTempo].concat(this.state.intervalTraining ? [this.defaultLowerTempo] : [])}
                value={this.state.currentTempoValue}
                min={60}
                max={200}
                aria-labelledby="discrete-slider-always"
                step={1}
                valueLabelDisplay="on"
                onChange={this.handleTempoInput} 
              />
            </Grid>
            <Grid item xs>
              <Button 
                variant="contained"
                onClick={this.getSongs}
                disabled={this.state.busy} 
                id="go"
              >Sko</Button>
            </Grid>
            <Grid item xs>
              {this.state.songs.length === 0 ? null : <Button 
                variant="contained"
                id="savePlaylist"
                onClick={this.makePlaylist}
              >Make it</Button>}
            </Grid>
            <Grid item xs>
              <SongList
                id="results" 
                songs={this.state.songs.map(x => ({track:x, disabled:false}))}
              />
            </Grid>
            <Grid item xs>
              <div style={{display:'inline-block',margin: '50px'}}>
                <MoonLoader
                  id="spinner"
                  size={50}
                  color={"#123abc"}
                  loading={this.state.busy}
                />
              </div>
            </Grid>
          </Grid>
        </div>
      )
    }
  }
  