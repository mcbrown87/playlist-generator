import React from 'react';
import './App.css';
import { createBrowserHistory } from "history";
import Grid from '@mui/material/Grid';
import PlaylistGenerator from './PlaylistGenerator'
import SongOfTheMorningPlaylist from './SongOfTheMorningPlaylist'
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import DirectionsRunIcon from '@mui/icons-material/DirectionsRun';
import ListIcon from '@mui/icons-material/List';

export default class App extends React.Component {
  constructor() {
    super()

    this.state = {
      navSelection: 0,
      welcomeText: ""
    }

    const history = createBrowserHistory();
    history.push("/");

    fetch('getUserData')
        .then(res => res.json()) 
        .then(res => {
          this.setState(() => ({
            welcomeText: `Welcome, ${res.display_name}`
          })) 
      }) 
  }

  render() {
    return (
      <div className="App">
        <Grid item xs>
          <h1 id="welcome-text">{this.state.welcomeText}</h1>
        </Grid>
        {this.state.navSelection === 0 ? <PlaylistGenerator /> : null}
        {this.state.navSelection === 1 ? <SongOfTheMorningPlaylist /> : null }
        <BottomNavigation
          value={this.state.navSelection}
          onChange={(event, newValue) => {
            this.setState(() => ({
              navSelection: newValue
            })) 
          }}
          showLabels
        >
          <BottomNavigationAction label="CreatePlaylist" icon={<DirectionsRunIcon />} />
          <BottomNavigationAction label="SOTM" icon={<ListIcon />} />
        </BottomNavigation>
      </div>
    )
  }
}
