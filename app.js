const express = require('express')
const path = require('path')
const querystring = require('querystring')
const SpotifyWebApi = require('spotify-web-api-node');
const Enumerable = require('linq')
require('dotenv').config()

const app = express()

app.use(express.static('client/build'))

var cookieParser = require('cookie-parser');
app.use(cookieParser());

const port = process.env.PORT
const showLoginDialog = process.env.SHOW_LOGIN_DIALOG || false

function getSpotifyAPI(accessToken) {
    let spotifyApi = new SpotifyWebApi({
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        redirectUri: `${process.env.SERVER_NAME}/spotify/callback`
        });

    if(accessToken) {
        spotifyApi.setAccessToken(accessToken)
    }

    return spotifyApi
}

class TrackQuerier {
    constructor(spotifyApi) {
        this._spotifyApi = spotifyApi
    }

    async getTracks(artist, genre, targetTempo, minTempo, maxTempo, tracksDuration = 1800000, totalTracks = [], maxDepth = 15) {
        console.log(`Getting tracks. Recursion depth left - ${maxDepth}`)
        if(Enumerable.from(totalTracks).sum(x => x.duration_ms) >= tracksDuration || maxDepth == 0) {
            return totalTracks
        }
        
        console.log(`Searching for artist '${artist}'`)
        let seed_artists = []
        if(artist) {
            seed_artists.push((await this._spotifyApi.search(artist, ["artist"])).body.artists.items[0].id)
        }

        let data = await this._spotifyApi.getRecommendations({seed_genres:[genre], seed_artists:seed_artists, min_danceability:.4, target_danceability:.8, target_tempo:targetTempo, min_tempo:minTempo, max_tempo:maxTempo})

        let tracks = Enumerable.from(data.body.tracks).union(totalTracks, x => x.id).toArray()

        if(Enumerable.from(tracks).sum(x => x.duration_ms) < tracksDuration && tracks.length != totalTracks.length) {
            let otherArtists = Enumerable.from(tracks).where(x => x.artists[0].toUpperCase() != artist.toUpperCase())
            let otherArtist = otherArtists.skip(Math.floor(Math.random() * otherArtists.count())).first().artist
            return await this.getTracks(otherArtist, genre, targetTempo, minTempo, maxTempo, tracksDuration, tracks, --maxDepth)
        }

        return tracks;
    }
}
    
app.get('/spotify/callback', (req, res) => {
    try {
        res.redirect(`/?code=${req.query.code}`)
    } catch (e) {
        res.status(500)
    } 
})

app.get('/getUserData', async (req, res) => {
    try {
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        res.send((await spotifyApi.getMe()).body)
    } catch (e) {
        console.log(e)
        res.status(500)
    }
})

app.post('/savePlaylist', async (req, res) => {
    try {
        let songs = req.query.songs.split(',')
        let playlistName = req.query.playlistName
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)

        let userId = (await spotifyApi.getMe()).body.id

        let playlistId = (await spotifyApi.createPlaylist(userId, playlistName, { 'public' : false })).body.id

        await spotifyApi.addTracksToPlaylist(playlistId, Enumerable.from(songs).select(x => `spotify:track:${x}`).toArray())
        res.sendStatus(200)
    } catch (e) {
        res.status(500)
    }
})

app.post('/createCleanPlaylist', async (req, res) => {
    try{
        let targetPlaylistId = req.query.targetPlaylistId
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let userId = (await spotifyApi.getMe()).body.id
        let playlistName = (await spotifyApi.getPlaylist(targetPlaylistId)).body.name

        let cleanPlaylistName = `${playlistName} (CLEAN)`
        let offset = 0
        let limit = 100
        let total = 0
        let tracks = [] 
        do {
            let res = (await spotifyApi.getPlaylistTracks(targetPlaylistId, { limit: limit, offset: offset }))
            total = res.body.total
            tracks = tracks.concat(res.body.items)
            offset += limit
        } while (limit + offset < total + limit)
                
        let cleanTracks = tracks.filter(x => !x.track.explicit)
        console.log(`Removed ${(((total - cleanTracks.length)/total)*100).toFixed(0)}% of tracks that are explicit`)

        let playlistId = (await spotifyApi.createPlaylist(userId, cleanPlaylistName, { 'public' : true })).body.id

        let chunk = 100
        let current = 0
        
        do {
            await spotifyApi.addTracksToPlaylist(playlistId, cleanTracks.slice(current, current + chunk > cleanTracks.length ? cleanTracks.length : current + chunk ).map(x => x.track.uri))
            current += 100
        } while(current < cleanTracks.length)        

    } catch (e) {
        res.status(500)
    }
})

app.get('/makePlaylist', async (req, res) => {
    try {
        let seedArtist = req.query.artist
        let genre = req.query.genre
        let targetTempo = parseInt(req.query.tempo)
        let playlistDuration = 30
        let tolerance = .01
        let minTempo = targetTempo - (targetTempo*tolerance)
        let maxTempo = targetTempo + (targetTempo*tolerance)
        
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let trackQuerier = new TrackQuerier(spotifyApi)

        let recommendationsAtTargetTempo = await trackQuerier.getTracks(seedArtist, genre, targetTempo, minTempo, maxTempo, playlistDuration)
        
        res.send(recommendationsAtTargetTempo)
    } catch (e) {
        console.log(e)
        res.status(500)
        res.send("error")
    } 
})

app.get('/getPlaylists', async (req, res) => {
    try {
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let userPlaylists = Enumerable.from((await spotifyApi.getUserPlaylists()).body.items)
        res.send(userPlaylists.toArray())
    } catch (e) {
        console.log(e)
        res.status(500)
    }
})

app.post('/addSongToPlaylist', async (req, res) => {
    try {
        let playlistId = req.query.playlistId
        let trackId = req.query.trackId

        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let playlistName = (await spotifyApi.getPlaylist(playlistId)).body.name
        
        let artistsInPlaylist = Enumerable.from((await spotifyApi.getPlaylistTracks(playlistId,{ limit: 100, offset: 0 })).body.items).select(x => x.track.artists[0].name).distinct()
        
        let track = (await spotifyApi.getTrack(trackId)).body
        let trackArtist = track.artists[0].name

        if (artistsInPlaylist.contains(trackArtist)) {
            res.status(500)
            res.send(`${trackArtist} already exists in ${playlistName}`)
        } else {
            await spotifyApi.addTracksToPlaylist(playlistId, [track.uri])
        }

        res.send("")
    } catch (e) {
        console.log(e)
        res.status(500)
    }
})

app.get('/getArtistsInPlaylist', async (req, res) => {
    try {
        let playlistId = req.query.playlistId

        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let offset = 0
        let tracks = []
        let response
        do {
            response = await spotifyApi.getPlaylistTracks(playlistId,{ limit: 100, offset: offset });
            tracks = tracks.concat(response.body.items)
            offset += 100
        } while(response.body.next != null)
 
        let artistsInPlaylist = Enumerable.from(tracks).select(x => x.track.artists[0].name).distinct().toArray()
        
        res.send(artistsInPlaylist)
        
    } catch(e) {
        console.log(e)
        res.status(500)
    }
})

app.get('/getGenres', async (req, res) => {
    try {
        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let genres = await spotifyApi.getAvailableGenreSeeds()
        res.send(genres.body.genres)
        
    } catch(e) {
        console.log(e)
        res.status(500)
        res.send("Failed")
    }
})

app.get('/searchMusic', async (req, res) => {
    try {
        let searchPattern = req.query.searchPattern

        let spotifyApi = await getSpotifyAPI(req.cookies.access_token)
        let searchResults = (await spotifyApi.search(searchPattern, ['track', 'artist'])).body.tracks.items
        res.send(searchResults)
        
    } catch(e) {
        console.log(e)
        res.status(500)
        res.send("Failed")
    }
})

app.get('/', async (req, res) => {
    try {
        if(!req.query.code) {
            res.clearCookie('access_token')
            res.redirect("https://accounts.spotify.com/authorize?" +
                    querystring.stringify({
                        client_id: process.env.CLIENT_ID,
                        scope: "user-read-email user-read-private playlist-modify-private playlist-modify-public",
                        redirect_uri: `${process.env.SERVER_NAME}/spotify/callback`,
                        response_type: "code",
                        show_dialog: showLoginDialog
                    })) 
        }
        let spotifyApi = getSpotifyAPI()
        let blah = await spotifyApi.authorizationCodeGrant(req.query.code)
        res.cookie('access_token', blah.body['access_token'])
        res.sendFile(path.join(__dirname, 'client/build/app.html'))
    } catch(e) {
        console.log(e)
        res.status(500)
    }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))