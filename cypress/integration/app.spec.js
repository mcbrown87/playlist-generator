/// <reference types="Cypress" />

describe("Playlist generator", () => {
  beforeEach(function () {
    cy.visit(Cypress.env("SERVER_NAME"))
    cy.get('#login-username').type(Cypress.env("USER_NAME"))
    cy.get('#login-password').type(Cypress.env("PASSWORD"), { log: false })
    cy.get('#login-button').click()
    cy.get('#auth-accept').click()
  })
  
  it('Should have correct title', () => {
    cy.title().should('eq', 'Playlist Generator')
  })

  it('Should not contain token in url after login', () => {
    cy.url().should("not.contain", "?code=")
  })

  it('Should display user name', () => {
    cy.get('#welcome-text').should("contain", "test account")
  })

  it('Should show results of query', () => {
    cy.get('#artist').type("blur")

    cy.get('#go').click()

    cy.get('#go').should('be.disabled')
    cy.get('.css-jcby4q')
    cy.get('.css-jcby4q').should('not.exist')
    cy.get('#results').children().should('have.length.greaterThan', 0)
  })

  it('Should play audio when engaging switch', () => {
    cy.get('#metronomeSwitch').should('not.be.checked')
    cy.get('#metronomeSwitch').click()
    cy.get('#metronomeSwitch').should('be.checked')
    cy.get('#metronomeSwitch').click()
    cy.get('#metronomeSwitch').should('not.be.checked')
  })

  it('Should show two sliders when engaging interval mode', () => {
    cy.get('#intervalTraining').should('not.be.checked')
    cy.get('span[role=slider]').should('have.length', 1)
    cy.get('#intervalTraining').click()
    cy.get('#intervalTraining').should('be.checked')
    cy.get('span[role=slider]').should('have.length', 2)
    cy.get('#intervalTraining').click()
    cy.get('#intervalTraining').should('not.be.checked')
    cy.get('span[role=slider]').should('have.length', 1)
  })
})